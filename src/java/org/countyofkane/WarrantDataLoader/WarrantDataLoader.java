package org.countyofkane.WarrantDataLoader;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author cicmnm
 */
@WebService(serviceName = "WarrantDataLoader")
public class WarrantDataLoader {

    /**
     * Get Warrant Data web service operation
     */
    @WebMethod(operationName = "GetWarrantData")
    public WarrantData GetWarrantData(@WebParam(name = "CaseNumber") String caseNumber, @WebParam(name = "PartyType") String partyType) throws Exception  {
          return WarrantDataCtlr.createWarrantDataObject(caseNumber, partyType);
    }
}
