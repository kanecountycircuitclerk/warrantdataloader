package org.countyofkane.WarrantDataLoader;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author cicmnm
 */
public class WarrantDataCtlr {
  
  //Variables
  private static XPath xpath = XPathFactory.newInstance().newXPath();
  private static Document txnResponse = null;
    
 /******************************************************************************************************************
 *
 * CREATE WARRANT DATA OBJECT
 *
 ******************************************************************************************************************/
    public static WarrantData createWarrantDataObject(String caseNumber, String partyType) throws Exception
    {
        //test odyssey txn execution DEBUG
        //System.out.println(odysseyTxnExecution(createOdysseyTxnXML(formatCaseNumber(caseNumber), partyType), "ILKANETEST")); //Tyler TEST
        //System.out.println(odysseyTxnExecution(createOdysseyTxnXML(formatCaseNumber(caseNumber), partyType), "ILKANEPROD")); //Tyler PROD
        
        //Create Document from Odyssey Transaction Execution Response
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(odysseyTxnExecution(createOdysseyTxnXML(formatCaseNumber(caseNumber), partyType), "ILKANEPROD"))); //Tyler PROD
        //is.setCharacterStream(new StringReader(odysseyTxnExecution(createOdysseyTxnXML(formatCaseNumber(caseNumber), partyType), "ILKANETEST"))); //Tyler TEST
        txnResponse = db.parse(is);
       
        
        //Create Warrant Data Object and set its values
        WarrantData wd = new WarrantData();
        wd.setCase(getCase());
        wd.setDefendant(getDefendant());
        wd.setCharges(getCharges());
        wd.setJurisdiction(getJurisdiction());
        

        //return warrant data object
        return wd;
    }
    
 /******************************************************************************************************************
 *
 * Format Case Number so it matches Tyler's Specification
 *
 ******************************************************************************************************************/
    
    private static String formatCaseNumber(String caseNumber) throws IllegalArgumentException{
        
        String tempYear, tempType, tempNumber;
        int numberLength=6;
        int inNumberLength=0;
        int loopNumberLength=0;
        
        //remove any spaces
        caseNumber = caseNumber.replaceAll(" ", "");
        //remove any dashes
        caseNumber = caseNumber.replaceAll("-", "");
        
        if(Pattern.compile("^\\d{2}[a-zA-Z]{2}\\d{1,6}").matcher(caseNumber).find()){
            
            //get year, type and number
            tempYear = caseNumber.substring(0, 2);
            tempType = caseNumber.substring(2, 4);
            tempNumber = caseNumber.substring(4);
            
            //pad number with leading zeros
            inNumberLength = tempNumber.length();
            loopNumberLength = numberLength - inNumberLength;
                
            for(int i=0; i < loopNumberLength; i++)
            {
                tempNumber = "0" + tempNumber;
            }
            
            //concat case number
            caseNumber = tempYear + tempType + tempNumber; 
              
                
        } else if(Pattern.compile("^\\d{4}[a-zA-Z]{2}\\d{1,6}").matcher(caseNumber).find()){
            
            //get year, type and number
            tempYear = caseNumber.substring(2, 4);  //start at 2 instead of 0 because we want to remove the century from the year. 
            tempType = caseNumber.substring(4, 6);
            tempNumber = caseNumber.substring(6);
            
            //pad number with leading zeros
            inNumberLength = tempNumber.length();
            loopNumberLength = numberLength - inNumberLength;
                
            for(int i=0; i < loopNumberLength; i++)
            {
                tempNumber = "0" + tempNumber;
            }
            
            //concat case number
            caseNumber = tempYear + tempType + tempNumber; 
               
        } else if(Pattern.compile("^\\d{2}[a-zA-Z]{1}\\d{1,6}").matcher(caseNumber).find()){
            
            //get year, type and number
            tempYear = caseNumber.substring(0, 2);
            tempType = caseNumber.substring(2, 3);
            tempNumber = caseNumber.substring(3);
            
            //pad Type with trailing space
            //tempType = tempType + " "; //API was fixed so I no longer need this
            
            //pad number with leading zeros
            inNumberLength = tempNumber.length();
            loopNumberLength = numberLength - inNumberLength;
                
            for(int i=0; i < loopNumberLength; i++)
            {
                tempNumber = "0" + tempNumber;
            }
            
            //concat case number
            caseNumber = tempYear + tempType + tempNumber; 
            
        } else if(Pattern.compile("^\\d{4}[a-zA-Z]{1}\\d{1,6}").matcher(caseNumber).find()){
            
            //get year, type and number
            tempYear = caseNumber.substring(2, 4); //start at 2 instead of 0 because we want to remove the century from the year.
            tempType = caseNumber.substring(4, 5);
            tempNumber = caseNumber.substring(5);
            
            //pad Type with trailing space
            //tempType = tempType + " ";  //API was fixed so I no longer need this
            
            //pad number with leading zeros
            inNumberLength = tempNumber.length();
            loopNumberLength = numberLength - inNumberLength;
                
            for(int i=0; i < loopNumberLength; i++)
            {
                tempNumber = "0" + tempNumber;
            }
            
            //concat case number
            caseNumber = tempYear + tempType + tempNumber; 
        }else{
            throw new IllegalArgumentException("Case Number is not in a valid format: " + caseNumber);
        }
        
        //retrun formated Case Number
        return caseNumber;
    }
    
    
 /******************************************************************************************************************
 *
 * GET VALUES FROM ODYSSEY TRANSACTION RESPONSE
 *
 ******************************************************************************************************************/
    
    //Get Case Information from Transaction Response
    private static WarrantData.Case getCase() throws Exception{
        WarrantData.Case c = new WarrantData.Case();
        c.setCaseID(getTxnResponseValue("/TxnResponse/Result/CaseID/text()"));
        c.setCaseNumber(getTxnResponseValue("/TxnResponse/Result/CaseNumber/text()"));
        c.setCaseStatus(getTxnResponseValue("/TxnResponse/Result/Case/CaseStatuses/CaseStatus[IsCurrent = 'true']/StatusType/text()"));
        c.setStateLocal(getStateLocal());
        return c;
    }
    
    //Get Defendant Information from Transaction Response
    private static WarrantData.Defendant getDefendant() throws Exception{
        WarrantData.Defendant def = new WarrantData.Defendant();
        def.setPartyID(getTxnResponseValue("/TxnResponse/Result/Party/PartyID/text()"));
        def.setFirstName(getTxnResponseValue("/TxnResponse/Result/Party/Names/Name/Person/First[../../IsCurrent = 'true']/text()"));
        def.setMiddleName(getTxnResponseValue("/TxnResponse/Result/Party/Names/Name/Person/Middle[../../IsCurrent = 'true']/text()"));
        def.setLastName(getTxnResponseValue("/TxnResponse/Result/Party/Names/Name/Person/Last[../../IsCurrent = 'true']/text()"));
        def.setAddress(getTxnResponseValue("/TxnResponse/Result[@MessageType='LoadParty']/Party/Addresses/Address/*/Line1[../../AddressFlags/CurrentKnown = 'true']/text()"));
        def.setCity(getTxnResponseValue("/TxnResponse/Result[@MessageType='LoadParty']/Party/Addresses/Address/*/City[../../AddressFlags/CurrentKnown = 'true']/text()"));
        def.setState(getTxnResponseValue("/TxnResponse/Result[@MessageType='LoadParty']/Party/Addresses/Address/*/State[../../AddressFlags/CurrentKnown = 'true']/text()"));
        def.setZip(getTxnResponseValue("/TxnResponse/Result[@MessageType='LoadParty']/Party/Addresses/Address/*/Zip[../../AddressFlags/CurrentKnown = 'true']/text()"));
        def.setDOB(getTxnResponseValue("/TxnResponse/Result/Party/DateOfBirths/DateOfBirth/Date[../IsCurrent = 'true']/text()"));
        def.setEyeColor(getTxnResponseValue("/TxnResponse/Result/Party/EyeColor/text()"));
        def.setHairColor(getTxnResponseValue("/TxnResponse/Result/Party/HairColor/text()"));
        def.setHeight(getHeight());
        def.setWeight(getTxnResponseValue("/TxnResponse/Result/Party/Weight/text()"));
        def.setRace(getTxnResponseValue("/TxnResponse/Result/Party/Race/text()"));
        def.setGender(getTxnResponseValue("/TxnResponse/Result/Party/Gender/text()"));
        def.setDLNumber(getTxnResponseValue("/TxnResponse/Result/Party/DriversLicenses/DriversLicense/Number[../IsCurrent = 'true']/text()"));
        def.setDLState(getTxnResponseValue("/TxnResponse/Result/Party/DriversLicenses/DriversLicense/DLState[../IsCurrent = 'true']/text()"));
        return def;
    }
    
    //Get Charge Information from Transaction Response
    private static WarrantData.Charges getCharges() throws Exception{
        WarrantData.Charges charges = new WarrantData.Charges();
        
        //Get Charge Nodes and store them in a NodeList
        NodeList nList = txnResponse.getElementsByTagName("Charge");
        
        //Find begining of active charges
        int sequenceNumber = 1;
        int beginActiveNumber = 0;
        for(int i = 0; i < nList.getLength(); i++){
            Node nNode = nList.item(i);
            if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                
                int tempSequenceNumber = Integer.parseInt(eElement.getElementsByTagName("ChargeTrackSequence").item(0).getTextContent());
                
                if(tempSequenceNumber > sequenceNumber){
                    sequenceNumber = tempSequenceNumber;
                }
                else{
                    sequenceNumber = 1;
                    beginActiveNumber = i;
                }
            }
        }
        
        //set number of charges
        charges.setNumberOfCharges(nList.getLength() - beginActiveNumber);
        
       
        //Set Charges
        while(beginActiveNumber < nList.getLength()){
            Node nNode = nList.item(beginActiveNumber);
            if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                WarrantData.Charges.Charge charge = new WarrantData.Charges.Charge();
                //System.out.println("ChargeID: " + eElement.getElementsByTagName("ChargeID").item(0).getTextContent() + "ChargeTrackSeauence: " + eElement.getElementsByTagName("ChargeTrackSequence").item(0).getTextContent());
                charge.setChargeID(eElement.getElementsByTagName("ChargeID").item(0).getTextContent());
                charge.setOffenseDate(eElement.getElementsByTagName("OffenseDate").item(0).getTextContent());
                charge.setDegree(eElement.getElementsByTagName("Degree").item(0).getTextContent());
                charge.setStatute(eElement.getElementsByTagName("Statute").item(0).getTextContent());
                charge.setDescription(eElement.getElementsByTagName("Description").item(0).getTextContent());
                
                //See if ChargeTrackNumber exists, and if it does, add it to the charge element
                if(eElement.getElementsByTagName("ChargeTrackNumber").getLength() != 0){
                    charge.setChargeTrackNumber(eElement.getElementsByTagName("ChargeTrackNumber").item(0).getTextContent());
                }
                
                charges.getCharge().add(charge);
            }
            beginActiveNumber++;
        }
        
        return charges;
    }
    
    
    //Get Jurisdiction Information from Transaction Response
    private static WarrantData.Jurisdiction getJurisdiction() throws Exception{
        WarrantData.Jurisdiction jurisdiction = new WarrantData.Jurisdiction();
        
        jurisdiction.setArrestingAgency(getTxnResponseValue("//TxnResponse/Result/Codes/Code[Word/text() = /TxnResponse/Result/Case/Charges/Charge/*/Jurisdiction]/Description/text()"));
        jurisdiction.setReportNumber(getTxnResponseValue("/TxnResponse/Result/Case/Charges/Charge[*/IsCurrent = 'true']/ArrestFiling/ArrestControlNumber")); //Not always there, may need to write code to handle it.
        jurisdiction.setComplainant(getTxnResponseValue("/TxnResponse/Result/Case/CaseParties/CaseParty[Connections/Connection/ConnectionType/text() = 'CM']/Names/PrimaryName/Person/First/text()") + " " +
                                    getTxnResponseValue("/TxnResponse/Result/Case/CaseParties/CaseParty[Connections/Connection/ConnectionType/text() = 'CM']/Names/PrimaryName/Person/Last/text()"));
        return jurisdiction;
        
    }
    
    
    
    //Get a specified value from the transaction response based on xPath expression
    private static String getTxnResponseValue(String xPathExpression){
        String value = null;
        
        try{
            XPathExpression expr = xpath.compile(xPathExpression);
            value = (String) expr.evaluate(txnResponse, XPathConstants.STRING);
            
        } catch (XPathExpressionException e){
            e.printStackTrace();
        }
        
        return value; 
    }
   
    
    //Get State/Local from Transaction Response
    private static String getStateLocal(){
        String stateLocal = null;
        
        try{
            XPathExpression expr = xpath.compile("/TxnResponse/Result/Case/Charges/Charge/*/Offense/Code[../../IsCurrent = 'true']/text()");
            stateLocal = (String) expr.evaluate(txnResponse, XPathConstants.STRING);
            if(stateLocal.contains("-S")){
                stateLocal = "S";
            } else {
                stateLocal = "L";
            }
            
        } catch (XPathExpressionException e){
            e.printStackTrace();
        }
        
        return stateLocal;
    }
   
    
    //Get Height from Transaction Response
    private static String getHeight(){
        String height = null;
        String feet = null;
        String inches = null;
        
        try{
            XPathExpression expr = xpath.compile("/TxnResponse/Result/Party/Height/Feet/text()");
            feet = (String) expr.evaluate(txnResponse, XPathConstants.STRING);
            expr = xpath.compile("/TxnResponse/Result/Party/Height/Inches/text()");
            inches = (String) expr.evaluate(txnResponse, XPathConstants.STRING);
            height = feet + "'" + inches +"\"";
            
        } catch (XPathExpressionException e){
            e.printStackTrace();
        }
        
        return height;
    }
    


    
/******************************************************************************************************************
 *
 * ODYSSEY API EXECUTION
 *
 ******************************************************************************************************************/

    private static String odysseyTxnExecution(java.lang.String odysseyTransactionXML, java.lang.String siteKey) {
        com.tylertech.APIWebService service = new com.tylertech.APIWebService();
        com.tylertech.APIWebServiceSoap port = service.getAPIWebServiceSoap();
        return port.odysseyTxnExecution(odysseyTransactionXML, siteKey);
    }
    
/******************************************************************************************************************
 *
 * CREATE ODYSSEY TRANSACTION XML STRING
 *
 ******************************************************************************************************************/
    
    private static String createOdysseyTxnXML(String caseNumber, String partyType){
        String txnXML = 
        "<Transaction ReferenceNumber=\"1\" Source=\"WarrantDataLoader\" TransactionType=\"WarrantDataLoader\">" +
		"<DataPropagation>" +
			"<IntraTxn xPath=\"/TxnResponse/Result[@MessageType='FindCaseByCaseNumber']/CaseID\" ReplStr=\"#|CaseID|#\" />" +
			"<IntraTxn xPath=\"/TxnResponse/Result[@MessageType='LoadCase']/Case/CaseParties/CaseParty/PartyID[../Connections/Connection/ConnectionType='" + partyType + "']\" ReplStr=\"#|PartyID|#\" />" +
		"</DataPropagation>" +
		"<Message MessageType=\"FindCaseByCaseNumber\" NodeID=\"1000\" ReferenceNumber=\"1\" UserID=\"1619\" Source=\"WarrantDataLoader\">" +
			"<CaseNumber>" + caseNumber + "</CaseNumber><SearchNodeID>1000</SearchNodeID>" +
		"</Message>" +
		"<Message MessageType=\"LoadCase\" NodeID=\"1000\" ReferenceNumber=\"1\" UserID=\"1619\" Source=\"WarrantDataLoader\">" +
			"<CaseID>#|CaseID|#</CaseID>" +
			"<LoadEntities>" +
				"<CaseParties>true</CaseParties>" +
				"<CaseEvents>true</CaseEvents>" +
				"<CaseStatuses>true</CaseStatuses>" +
				"<Charges>true</Charges>" +
				"<CaseCrossReferences>true</CaseCrossReferences>" +
				"<CaseFlags>true</CaseFlags>" +
			"</LoadEntities>" +
		"</Message>" +
		"<Message MessageType=\"LoadParty\" NodeID=\"0\" ReferenceNumber=\"1\" UserID=\"1619\" Source=\"WarrantDataLoader\">" +
			"<PartyID>#|PartyID|#</PartyID>" +
			"<CurrentKnownOnly>false</CurrentKnownOnly>" +
			"<MaxNumberOfResults>2</MaxNumberOfResults>" +
			"<LoadEntities>" +
				"<CaseManagerCases>true</CaseManagerCases>" +
				"<DateOfBirth>true</DateOfBirth>" +
				"<SONumber>true</SONumber>" +
				"<StateID>true</StateID>" +
				"<DLNumber>true</DLNumber>" +
				"<PersonID>true</PersonID>" +
				"<FBINumber>true</FBINumber>" +
				"<OtherAgencyNumber>true</OtherAgencyNumber>" +
				"<SMT>true</SMT>" +
				"<PhysicalDescriptor>true</PhysicalDescriptor>" +
				"<Schools>true</Schools>" +
				"<Groups>true</Groups>" +
				"<CautionFlags>true</CautionFlags>" +
				"<SpecialConditions>true</SpecialConditions>" +
				"<Language>true</Language>" +
				"<SexOffenderRegistration>true</SexOffenderRegistration>" +
				"<Miscellaneous>true</Miscellaneous>" +
				"<Name>true</Name>" +
				"<Address>true</Address>" +
				"<Phone>true</Phone>" +
				"<Officer>true</Officer>" +
			"</LoadEntities>" +
		"</Message>" +
		"<Message MessageType=\"GetCodeListing\" NodeID=\"1\" ReferenceNumber=\"1\" UserID=\"1619\" Source=\"WarrantDataLoader\">" +
			"<CodeTypeID>JUS150</CodeTypeID>" +
			"<ShowObsolete>True</ShowObsolete>" +
			"<ShowHidden>True</ShowHidden>" +
			"<ShowNonEffective>True</ShowNonEffective>" +
			"<ShowAllNodes>True</ShowAllNodes>" +
			"<IncludeResults>" +
				"<Word>True</Word>" +
				"<Description>True</Description>" +
			"</IncludeResults>" +
		"</Message>"+
	"</Transaction>";
        

        //return Txn XML string
        return txnXML;
    }
    
    
    
}
